/**
 * BidTest.java
 */

package org.com1027.coursework.q1;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Alexis Chrysostomou
 *
 */

public class BidTest {

	/**
	 * Creating a Bid Object and testing whether the field values are correctly
	 * assigned.
	 */
	@Test
	public void testBidConstructor() {
		User user = new User("User");
		Bid bid = new Bid(user, 100.1);
		assertEquals("User", bid.getBuyer().toString());
		assertEquals(100.1, bid.getBidValue(), 0.1);
	}

	/**
	 * Creating a Bid Object and testing whether the appropriate exceptions for the
	 * user parameter
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidUser() {
		Bid bid = new Bid(null, 100.1);
		assertEquals(null, bid.getBuyer().toString());
	}
	
	/**
	 * Creating a Bid Object and testing whether the appropriate exceptions for the
	 * bidValue parameter
	 */
	@Test(expected = IllegalArgumentException.class)
	public void testInvalidBidValue() {
		User user = new User("User");
		Bid bid = new Bid(user, -11);
		assertEquals(-11, bid.getBidValue(), 0.1);
	}
}

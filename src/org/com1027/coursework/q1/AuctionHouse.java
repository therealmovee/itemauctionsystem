package org.com1027.coursework.q1;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class AuctionHouse {

	/**
	 * Map Field used to store products that are currently for sale
	 */
	private Map<Product, User> forSaleProducts = new HashMap<Product, User>();

	/**
	 * Map Field used to store products that are already sold.
	 */
	private Map<Product, User> soldProducts = new HashMap<Product, User>();

	/**
	 * Map Field used to store products that are were not sold yet.
	 */
	private Map<Product, User> unsoldProducts = new HashMap<Product, User>();

	/**
	 * Checks if a product exists in any of the three hashMaps
	 * 
	 * @param product The product that we want to check if it exists
	 * @return True/False depending on if the product exists
	 */
	public boolean checkExistence(Product product) {
		// Preventing null error.
		if (product == null)
			throw new IllegalArgumentException("Null Product Object");

		// Check if product exists in unsoldProducts hashMap
		for (Entry<Product, User> products : this.unsoldProducts.entrySet()) {
			if (product == products.getKey()) {
				return true;
			}
		}

		// Check if product exists in forSaleProducts hashMap
		for (Entry<Product, User> products : this.forSaleProducts.entrySet()) {
			if (product == products.getKey()) {
				return true;
			}
		}

		// Check if product exists in soldProducts hashMap
		for (Entry<Product, User> products : this.soldProducts.entrySet()) {
			if (product == products.getKey()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @return All the sold products appended in a string.
	 */
	public String displaySoldProducts() {
		StringBuffer stringBuffer = new StringBuffer();

		// Loop through all the entries of the soldProducts hashMap
		for (Entry<Product, User> soldProduct : this.soldProducts.entrySet()) {
			// Appending all soldProducts into a single string
			stringBuffer.append(soldProduct.getKey().getProductId());
			stringBuffer.append(" - ");
			stringBuffer.append(soldProduct.getKey().getHighestBid().toString());
			stringBuffer.append("\n");
		}

		return stringBuffer.toString();
	}

	/**
	 * @return All the non-sold products appended in a string.
	 */
	public String displayUnsoldProducts() {
		StringBuffer stringBuffer = new StringBuffer();

		// Loop through all the entries of the soldProducts hashMap
		for (Entry<Product, User> unsoldProduct : this.unsoldProducts.entrySet()) {
			stringBuffer.append(unsoldProduct.getKey().getProductId());
			stringBuffer.append(" - ");
			stringBuffer.append(unsoldProduct.getKey().getProductName());
			stringBuffer.append("\n");
		}

		return stringBuffer.toString();
	}

	/**
	 * Removes a product from the Auction placing it on soldProducts or the
	 * unsoldProducts hashMap depending on if the highest bid exceeded the reserved
	 * price
	 * 
	 * @param product The product that will be removed from the auction
	 */
	public void endAuction(Product product) {
		if (product == null)
			throw new IllegalArgumentException("Invalid Product.");

		// Removing the product from forSaleProducts
		for (Entry<Product, User> products : this.forSaleProducts.entrySet()) {
			if (product == products.getKey()) {
				// Checking if there is a bid that is higher than the reserved price
				if (product.getHighestBid() != null
						&& product.getHighestBid().getBidValue() > product.getReservedPrice()) {
					this.soldProducts.put(product, products.getKey().getHighestBid().getBuyer());
				} else {
					this.unsoldProducts.put(product, products.getValue());
				}

				break;
			}
		}

		this.forSaleProducts.remove(product);
	}

	/**
	 * Places a bid on a product, only if the bid value is higher than the current
	 * highest bid
	 * 
	 * @param product  The product that the bid will be placed on
	 * @param user     The user who is bidding
	 * @param bidValue The value that the user is bidding
	 * @return True/False depending on if the bid was successful
	 */
	public boolean placeBid(Product product, User user, double bidValue) {
		// Validation checks on the parameters
		if (product == null || user == null || bidValue <= 0)
			throw new IllegalArgumentException("Invalid Parameters");

		// Checking if the product exists
		if (!checkExistence(product))
			return false;

		// Checking if there any other previous bids
		if (product.getHighestBid() == null) {
			product.placeBid(user, bidValue);

		} else {
			// Checking if the bidValue is higher than the current bid value of the product
			if (product.getHighestBid().getBidValue() > bidValue)
				return false;

			product.placeBid(user, bidValue);
		}

		return true;
	}

	/**
	 * Adds the product to the auction
	 * 
	 * @param product The product that will be added to the auction
	 * @param user    The user who is selling the product
	 * @return True/False depending on if the product registration was successful
	 */
	public boolean register(Product product, User user) {
		// Validation checks on parameters
		if (product == null || user == null)
			throw new IllegalArgumentException("Invalid parameters.");

		// Checking if the product already exists
		if (checkExistence(product))
			return false;

		this.forSaleProducts.put(product, user);

		return true;
	}
}

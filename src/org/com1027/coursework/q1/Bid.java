/**
 * Bid.java
 */

package org.com1027.coursework.q1;

/**
 * @author Alexis Chrysostomou
 */

public class Bid {

	/**
	 * Field Used To Store A User's Object
	 */
	private User buyer = null;

	/**
	 * Field Used To Store A User's Bid Value
	 */
	private double bidValue = 0;

	/**
	 * Bid Constructor
	 * 
	 * @param buyer    The User Who Is Bidding
	 * @param bidValue The Bid Value That The User Is Bidding
	 */
	public Bid(User buyer, double bidValue) {
		super();

		// Additional Checks On The Parameters
		checkParameters(buyer, bidValue);

		this.buyer = buyer;
		this.bidValue = bidValue;
	}

	/**
	 * @return The Current Bid Value
	 */
	public double getBidValue() {
		return this.bidValue;
	}

	/**
	 * @return The User That Is Bidding
	 */
	public User getBuyer() {
		return this.buyer;
	}

	/**
	 * Overrides the default toString() method
	 */
	@Override
	public String toString() {
		return this.buyer.toString() + " bid �" + this.bidValue;
	}

	/**
	 * Method that validates the parameters given in the constructor
	 * 
	 * @param buyer    The user who is bidding for a product
	 * @param bidValue The bid value of the user
	 */
	private void checkParameters(User buyer, double bidValue) {
		// Check if the buyer object is null
		if (buyer == null)
			throw new IllegalArgumentException("Null Buyer Parameter.");

		// Check if bid value is within accepted boundaries.
		if (bidValue < 0)
			throw new IllegalArgumentException("Invalid bidValue Parameter.");
	}

}

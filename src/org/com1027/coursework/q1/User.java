/**
 * User.java
 */

package org.com1027.coursework.q1;

/**
 * @author Alexis Chrysostomou
 */

public class User {

	/**
	 * Field used to store the user's name
	 */
	private String name = null;

	/**
	 * User Constructor
	 * 
	 * @param name Defines the user name
	 */
	public User(String name) {
		super();

		// Additional checks on the name parameter
		if (name == null)
			throw new IllegalArgumentException("Null Username Parameter.");

		this.name = name;
	}

	/**
	 * Overrides the default toString() method
	 */
	@Override
	public String toString() {
		return this.name;
	}

}
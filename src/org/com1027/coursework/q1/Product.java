package org.com1027.coursework.q1;

import java.util.ArrayList;
import java.util.List;

public class Product {

	/**
	 * Field used to store a product's ID
	 */
	private int productId;

	/**
	 * Field used to store a product's name
	 */
	private String productName = null;

	/**
	 * Field used to store a product's reserved price
	 */
	private double reservedPrice = 0;

	/**
	 * List used to store all the bids
	 */
	private List<Bid> bids = null;

	/**
	 * Constructor
	 * 
	 * @param productId     The product's ID
	 * @param productName   The product's name
	 * @param reservedPrice The product's price
	 */
	public Product(int productId, String productName, double reservedPrice) {
		super();
		checkParameters(productId, productName, reservedPrice);

		this.productId = productId;
		this.productName = productName;
		this.reservedPrice = reservedPrice;
		this.bids = new ArrayList<Bid>();
	}

	/**
	 * @return Bid Object with the highest bid amount
	 */
	public Bid getHighestBid() {
		double highestBid = this.bids.size() > 0 ? this.bids.get(0).getBidValue() : 0;
		int bidIndex = 0;

		// Getting the index of the highest Bid object
		for (int i = 0; i < this.bids.size(); i++) {
			if (this.bids.get(i).getBidValue() > highestBid) {

				highestBid = this.bids.get(i).getBidValue();
				bidIndex = i;
			}
		}

		return this.bids.size() > 0 ? this.bids.get(bidIndex) : null;
	}

	/**
	 * @return The product's ID
	 */
	public int getProductId() {
		return this.productId;
	}

	/**
	 * @return The product's Name
	 */
	public String getProductName() {
		return this.productName;
	}

	/**
	 * @return The product's reservedPrice
	 */
	public double getReservedPrice() {
		return this.reservedPrice;
	}

	/**
	 * Method that allows a user to place a bid value as long as its higher than the
	 * current bid value
	 * 
	 * @param user     The user who is trying to place a bid.
	 * @param bidValue The amount he is bidding.
	 * @return True/False depending on if he has the highest bid amount.
	 */
	public boolean placeBid(User user, double bidValue) {
		// Additional Checks on parameters
		if (user == null || bidValue <= 0)
			throw new IllegalArgumentException("Invalid User.");

		// Adding a temporary bid space in the arrayList
		// which we will later change if it's empty
		if (this.getHighestBid() == null)
			this.bids.add(new Bid(user, 0));

		Bid highestBid = this.getHighestBid();

		if (bidValue >= highestBid.getBidValue()) {
			// Removing The Previous Bid
			this.bids.remove(highestBid);

			// Adding The New Bid
			this.bids.add(new Bid(user, bidValue));

			return true;
		}

		return false;
	}
	
	/**
	 * Method that validates its parameters and throws the appropriate exceptions
	 * when necessary
	 * 
	 * @param productId     The product's ID
	 * @param productName   The product's name
	 * @param reservedPrice The product's price
	 */
	private void checkParameters(int productId, String productName, double reservedPrice) {
		// Checking the Validity of productId
		if (productId < 0)
			throw new IllegalArgumentException("Invalid Product ID");

		// Ensuring That productName Is Not Null
		if (productName == null)
			throw new IllegalArgumentException("Invalid Product Name");

		// Checking The Validity Of reservedPrice
		if (reservedPrice < 0)
			throw new IllegalArgumentException("Invalid Reserved Price");
	}
}
